
/**
requires d3v4
**/
function drawSimpleLine() {
	// set the dimensions and margins of the graph
var margin = {top: 20, right: 20, bottom: 30, left: 50},
    width = $("#graph2").parent().width() - margin.left - margin.right,
    height = $("#graph2").parent().height() - margin.top - margin.bottom;
// parse the date / time
var parseTime = d3.timeParse("%d-%b-%y");
// set the ranges
var x = d3.scaleTime().range([0, width]);
var y = d3.scaleLinear().range([height, 0]);
// define the line
var valueline = d3.line()
    .x(function(d) { return x(d.date); })
    .y(function(d) { return y(d.close); });
// append the svg object to the body of the page
// appends a 'group' element to 'svg'
// moves the 'group' element to the top left margin
var svg = d3.select("#graph2").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform",
          "translate(" + margin.left + "," + margin.top + ")");
// Get the data
d3.csv("/static/data/data.csv", function(error, data) {
  if (error) throw error;
// format the data
  data.forEach(function(d) {
      d.date = parseTime(d.date);
      d.close = +d.close;
  });
// Scale the range of the data
  x.domain(d3.extent(data, function(d) { return d.date; }));
  y.domain([0, d3.max(data, function(d) { return d.close; })]);
// Add the valueline path.
  svg.append("path")
      .data([data])
      .attr("class", "linearline")
      .attr("d", valueline);
// Add the X Axis
  svg.append("g")
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(x));
// Add the Y Axis
  svg.append("g")
      .call(d3.axisLeft(y));
});
}

function drawWordCloud(id) {
	// word cloud

	var svg = d3.select(id);
	var width = $(id).parent().width();
	var height = $(id).parent().height();

	var color = d3.scaleOrdinal(d3.schemeCategory20);

	var simulation = d3.forceSimulation()
		.force("link", d3.forceLink().id(function(d) { return d.id; }))
	.force("charge", d3.forceManyBody().strength(-600).distanceMax(200).distanceMin(10))
		.force("center", d3.forceCenter(width / 2, height / 2));

	d3.json("/static/data/got_interactions.json", function(error, graph) {
	  if (error) throw error;

	  var link = svg.append("g")
		  .attr("class", "links")
		.selectAll("line")
		.data(graph.links)
		.enter().append("line")
		  .attr("stroke-width", function(d) { return d.value/170; });

	  var nodes = svg.append("g")
		  .attr("class", "nodes")
		.selectAll("circle")
		.data(graph.nodes)
		.enter().append("g")
	  
	  nodes.append("circle")
		  .attr("r", 5)
		  .attr("fill", function(d) { return color(d.group); })
		   .call(d3.drag()
			 .on("start", dragstarted)
			 .on("drag", dragged)
			 .on("end", dragended));
	  
	  nodes.append("text")
		.attr("dx", 8)
		.attr("dy", ".35em")
		.text(function(d) { return d.id });
	  
	  simulation
		  .nodes(graph.nodes)
		  .on("tick", ticked);

	  simulation.force("link")
		  .links(graph.links);

	  function ticked() {
		link
			.attr("x1", function(d) { return d.source.x; })
			.attr("y1", function(d) { return d.source.y; })
			.attr("x2", function(d) { return d.target.x; })
			.attr("y2", function(d) { return d.target.y; });

		nodes
			.attr("transform", function(d) { return "translate("+[d.x,d.y]+")" });
	  }
	});

	function dragstarted(d) {
	  if (!d3.event.active) simulation.alphaTarget(0.3).restart();
	  d.fx = d.x;
	  d.fy = d.y;
	}

	function dragged(d) {
	  d.fx = d3.event.x;
	  d.fy = d3.event.y;
	}

	function dragended(d) {
	  if (!d3.event.active) simulation.alphaTarget(0);
	  d.fx = null;
	  d.fy = null;
	}
}

function xhRefG(url){
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			return this.responseText;
		}
	};
	xhttp.open("GET", url, true);
	xhttp.send();
}

$(document).ready(function(){
	$("#chartDataButton").click(function(){$('#tdbli').removeClass();$("#tableDataButton").parent().addClass('active');$("#tableData").fadeToggle("slow");$("#chartData").fadeToggle("slow");});
	$("#tableDataButton").click(function(){$('#cdbli').removeClass();$("#chartDataButton").parent().addClass('active');$("#tableData").fadeToggle("slow");$("#chartData").fadeToggle("slow");getTopicTableData();});
	function drawGroupedBar(data) {
		nv.addGraph(function() {
		var chart = nv.models.multiBarChart()
		  .duration(350)
		  .reduceXTicks(false)   //If 'false', every single x-axis tick label will be rendered.
		  .rotateLabels(20)      //Angle to rotate x-axis labels.
		  .showControls(true)   //Allow user to switch between 'Grouped' and 'Stacked' mode.
		  .groupSpacing(0.1)    //Distance between each group of bars.
		  .width($("#graph1").parent().width())
		  .height($("#graph1").parent().height())
		;

		chart.xAxis
			.tickFormat(function(d){var label="";
			for(i in data[0]['values']){
				if(data[0]['values'][d]['x']==d){label=data[0]['values'][d]['label'];}} return label;});

		chart.yAxis
			.tickFormat(d3.format(',.1f'));

		d3.select('#graph1 svg')
			.datum(data)
			.call(chart);

		nv.utils.windowResize(chart.update);
		chart.multibar.dispatch.on("elementClick", function(e) {
			$("#detailsText").html('<h3>Details for <strong>'+e.data.label+'</strong></h3>');
			setTimeout(function(){callDetailUpdate(e.data.label);},500);
		});
		return chart;
		});
		selectedTopic = data[0]['values'][0].label;
		setTimeout(function(){callDetailUpdate(data[0]['values'][0].label,true);},1000);
	}
	function drawPieChart(id,data){
		nv.addGraph(function() {
			var width=350,height=350;
        var chart = nv.models.pieChart()
            .x(function(d) { return d.key })
            .y(function(d) { return d.y })
			.width(width)
			.height(height)
            .showTooltipPercent(true);

        d3.select('#'+id+' svg')
            .datum(data)
            .transition().duration(1200)
			.attr("width",width)
			.attr("height",height)
            .call(chart);

        return chart;
    });
	}
	
	function xhRef(urlPath){var res=[];$.ajax({async:false,url:urlPath}).done(function(a) {res = a;}).fail(function() {return [];}).always(function() {});return res;}
	function callDetailUpdate(label,skip) {
		if(label == selectedTopic && !skip){}else{
			$(".loader").css("visibility","visible");
		var names=["hebWeb","inMoment","surveyAid"];
		for(i in names){
			$("#"+names[i]).text("");
		};
		$("#detailsText").html('<h3>Details for <strong>'+label+'</strong></h3>');
		data = xhRef('/getTopicToSurveys?topic='+label);
		for(i in data){
			$("#"+data[i].label).text(data[i].value);
		};
		drawPieChart("graph2",data);
		$(".loader").css("visibility","hidden");}
	}
	
	var selectedTopic = "";
	
	if(window.location.pathname=="/"){
	drawGroupedBar(xhRef("/getSentimentTopics?limit="+$("#topProducts").val()));}
	var topicSentimentStr=''
	function getTopicTableData(){
	var tableData=xhRef("/get-topics-all")
	var str=''
	for(i in tableData) {
		str=str+'<tr><td>'+tableData[i].topic+'</td><td>'+tableData[i].negative+'</td><td>'+tableData[i].neutral+'</td><td>'+tableData[i].positive+'</td><td>'+tableData[i]['InMoment']+'</td><td>'+tableData[i]['HEB.com']+'</td><td>'+tableData[i]['Survey-Aid']+'</td></tr>'
	}topicSentimentStr=str;$("#topicData").html(str);}
	
	$("#topProducts").change(function(){console.log($(this).val());drawGroupedBar(xhRef("/getSentimentTopics?limit="+$("#topProducts").val()));})
	$("#downloadOption").change(function(){
		if($("#downloadOption").val()=='Enriched Data'){$("#topicData").html("<h3>Please download to view as data is too large to display.</h3>");}
		else if($("#downloadOption").val()=='Topic Sentiment Data'){$("#topicData").html(topicSentimentStr);}
	});
	$("#downloadButton").click(function(){if($("#downloadOption").val()=="Topic Sentiment Data"){window.location='/download/alltopics';}else if($("#downloadOption").val()=="Enriched Data"){window.location='/download/enrichedData';}});
	
	if(window.location.pathname=='/summarizeTool'){
		$("#computeSummary").click(function(){var d=xhRef("/summarize?summary="+$("#basic-summary").val())
			for(i in d){$("#summaryLength").text($("#basic-summary").val().length + " words to " + d[i]['0'].length+" words");$("#summary").text(d[i]['0']);}
		})
		drawPieChart("graph2",xhRef("/summaryStats"))
	}
})