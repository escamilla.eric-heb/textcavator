from flask import Flask, flash, redirect, render_template, request, session, abort,send_from_directory,send_file,jsonify,Markup,make_response
from io import BytesIO
import pandas as pd
import requests
import xlsxwriter
import datetime
import mimetypes
import json

import json

#1. Declare application
appName="Textcavator UI"
application= Flask(appName)

DATABRICKS_TOKEN = 'dapi15dedfdd9ea928f4f9104ff9dd456db5'

def cmp(a, b):
	return (a > b) - (a < b) 

#2. Declare data stores
class Topic():
	rank=0
	topicName="na"

@application.route("/",methods=["GET"])
def homepage():
	data = ''
	with open('templates/navbar.html', 'r') as file:
		data = file.read().replace('\n', '')
	return render_template("index.html",appName=appName,navBar=Markup(data))

@application.route("/getSentimentDates",methods=["GET"])
def getSentimentByDates():
	data = ''
	with open('templates/navbar.html', 'r') as file:
		data = file.read().replace('\n', '')
	return render_template("sentimentByDate.html",appName=appName,navBar=Markup(data))

@application.route("/summarizeTool",methods=["GET"])
def summarize():
	data = ''
	with open('templates/navbar.html', 'r') as file:
		data = file.read().replace('\n', '')
	return render_template("summarize.html",appName=appName,navBar=data)
	
@application.route("/sentiment",methods=["GET"])
def sentiment():
	data = ''
	with open('templates/navbar.html', 'r') as file:
		data = file.read().replace('\n', '')
	return render_template("chart_gensim.html",appName=appName,navBar=data)

@application.route("/topicNetwork",methods=["GET"])
def topicNetwork():
	data = ''
	with open('templates/navbar.html', 'r') as file:
		data = file.read().replace('\n', '')
	return render_template("topicNetwork.html",appName=appName,navBar=data)

@application.route("/download/alltopics",methods=["GET"])
def downloadTopics():
	topics = pd.read_csv("static/data/sentiment_topic.csv",header=0)
	heb=pd.read_parquet("static/data/heb.parq",columns=['source_name','date','sentiment_label','text','summary'])
	surveyaid=pd.read_parquet("static/data/survey_aid.parq",columns=['source_name','date','sentiment_label','text','summary'])
	inmoment=pd.read_parquet("static/data/inmoment.parq",columns=['source_name','date','sentiment_label','text','summary'])
	all=pd.concat([heb,surveyaid,inmoment])
	
	list = []
	
	for topic in topics.topic.unique():
		restmp = {}
		text = all[all['text'].str.contains(topic) & (all.text.isna()==False)]
		for k,v in text.groupby("source_name").size().items():
			restmp[k]=v
		for k,v in text.groupby("sentiment_label").size().items():
			restmp[k]=v
		restmp['topic']=topic
		list.append(restmp)
	
	try:
		response = make_response()
		output = BytesIO()
		workbook = xlsxwriter.Workbook(output, {'in_memory': True})
		worksheet = workbook.add_worksheet('topic_occurrences')
		row = 0
		col = 0
		for k in ["Topic Term","Negative Reviews","Positive Reviews","Neutral Reviews","HEB.com Reviews","InMoment Reviews","Survey-Aid Reviews"]:
			worksheet.write(row,col,k)
			col = col+1
		col = 0
		row = row+1
		for i in list:
			worksheet.write(row,col,i['topic'])
			worksheet.write(row,col+1,i['negative'] if 'negative' in i else 0)
			worksheet.write(row,col+2,i['positive'] if 'positive' in i else 0)
			worksheet.write(row,col+3,i['neutral'] if 'neutral' in i else 0)
			worksheet.write(row,col+4,i['HEB.com'] if 'HEB.com' in i else 0)
			worksheet.write(row,col+5,i['InMoment'] if 'InMoment' in i else 0)
			worksheet.write(row,col+6,i['Survey-Aid'] if 'Survey-Aid' in i else 0)
			row=row+1
		workbook.close()
		output.seek(0)
		response.data = output.read()
		file_name = 'topic_data_{}.xlsx'.format(datetime.datetime.now().strftime('%d/%m/%Y'))
		mimetype_tuple = mimetypes.guess_type(file_name)
		response_headers = {'Pragma': "public",'Expires': '0','Cache-Control': 'must-revalidate, post-check=0, pre-check=0','Cache-Control': 'private','Content-Type': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','Content-Disposition': 'attachment; filename=\"%s\";' % file_name,'Content-Transfer-Encoding': 'binary','Content-Length': len(response.data)}

		#if not mimetype_tuple[1] is None:
		#	response.update({'Content-Encoding': mimetype_tuple[1]})
		response.headers = response_headers
		#response.set_cookie('fileDownload', 'true', path='/')
		return response
	except Exception as e:
		print(e)
		return "error "+str(e)

@application.route("/get-topics-all",methods=["GET"])
def returnTopics():
	topics = pd.read_csv("static/data/sentiment_topic.csv",header=0)
	heb=pd.read_parquet("static/data/heb.parq",columns=['source_name','date','sentiment_label','text','summary'])
	surveyaid=pd.read_parquet("static/data/survey_aid.parq",columns=['source_name','date','sentiment_label','text','summary'])
	inmoment=pd.read_parquet("static/data/inmoment.parq",columns=['source_name','date','sentiment_label','text','summary'])
	all=pd.concat([heb,surveyaid,inmoment])
	
	list = []
	
	for topic in topics.topic.unique():
		restmp = {'topic':topic,'rank':0,'negative':0,'neutral':0,'positive':0,
		'heb':0,'inmoment':0,'survey_aid':0}
		text = all[all['text'].str.contains(topic) & (all.text.isna()==False)]
		for k,v in text.groupby("source_name").size().items():
			restmp[k]=v
		for k,v in text.groupby("sentiment_label").size().items():
			restmp[k]=v
		list.append(restmp);
	return jsonify(list);

@application.route("/hebEnriched",methods=["GET"])
def returnHebEnriched():
	df1 = pd.read_parquet("static/data/heb.parq")
	count = 0
	return jsonify({'count':len(df1)});

@application.route("/inmomentEnriched",methods=["GET"])
def returnInmomentEnriched():
	df1 = pd.read_parquet("static/data/inmoment.parq")
	count = 0
	return jsonify({'count':len(df1)});

@application.route("/surveyAidEnriched",methods=["GET"])
def returnSurveyAidEnriched():
	df1 = pd.read_parquet("static/data/survey_aid.parq")
	count = 0
	return jsonify({'count':len(df1)});

@application.route("/summarize",methods=["GET"])
def summarizeDescription():
	sample_text = request.args.get("summary")
	sent_len = len(sample_text.split())
	dataset = {"text_adj": sample_text, 'sent_len': sent_len}
	url = 'https://adb-5983567243184270.10.azuredatabricks.net/model/huggingface_summarizer/5/invocations'
	headers = {'Authorization': 'Bearer '+DATABRICKS_TOKEN}
	data_json = dataset.to_dict(orient='split') if isinstance(dataset, pd.DataFrame) else [dataset]  # If the input is simply a string, then wrap the python dict in to a list
	response = requests.request(method='POST', headers=headers, url=url, json=data_json)
	if response.status_code != 200:
		print('Request failed with status '+str(response.status_code)+', '+response.text)
	return jsonify(response.json())

@application.route("/summaryStats",methods=["GET"])
def summaryStats():
	runHeavy=False
	if runHeavy:
		heb=pd.read_parquet("static/data/heb.parq",columns=['source_name','date','sentiment_label','text','summary','Own_Brand_Switch'])
		surveyaid=pd.read_parquet("static/data/survey_aid.parq",columns=['source_name','date','sentiment_label','text','summary'])
		inmoment=pd.read_parquet("static/data/inmoment.parq",columns=['source_name','date','sentiment_label','text','summary'])
		all=pd.concat([heb,surveyaid,inmoment])
		all=all[all.text.isna()==False]
		results=[]
		prelim=[]
		for i,r in all.iterrows():
			if r['text'] is None:
				arb=0
			if r['summary'] is None:
				arb=0
			else:
				prelim.append({'length':len(r['text']),'new':len(r['summary']),'percent':len(r['summary'])/len(r['text'])})
		percentDict={'0to10':0,'10to20':0,'20to30':0,'30to40':0,'40to50':0}
		for i in prelim:
			tmp=i['percent']
			if cmp(tmp*100,10.0) == -1:
				percentDict['0to10']=percentDict['0to10']+1
			if cmp(tmp*100,20.0) == -1 & cmp(tmp*100,10.0) == 1:
				percentDict['10to20']=percentDict['10to20']+1
			if cmp(tmp*100,30.0) == -1 & cmp(tmp*100,20.0) == 1:
				percentDict['20to30']=percentDict['20to30']+1
			if cmp(tmp*100,20.0) == -1 & cmp(tmp*100,30.0) == 1:
				percentDict['30to40']=percentDict['30to40']+1
			if cmp(tmp*100,50.0) == -1 & cmp(tmp*100,40.0) == 1:
				percentDict['40to50']=percentDict['40to50']+1
		for i in percentDict:
			results.append({'key':i,'y':percentDict[i]})
		return jsonify(results)
	else:
		return jsonify(json.loads('[{"key": "0to10","y": 381}, { "key": "10to20", "y": 13260}, {"key": "20to30", "y": 12044}, {"key": "30to40","y": 12044},{"key": "40to50","y": 8548}]'))

@application.route("/getSentimentTopics",methods=["GET","POST"])
def returnSentimentTopics():
	limit = request.args.get("limit")
	topics = pd.read_csv("static/data/sentiment_topic.csv",header=0)
	heb=pd.read_parquet("static/data/heb.parq",columns=['source_name','date','sentiment_label','text','summary','Own_Brand_Switch'])
	surveyaid=pd.read_parquet("static/data/survey_aid.parq",columns=['source_name','date','sentiment_label','text','summary'])
	inmoment=pd.read_parquet("static/data/inmoment.parq",columns=['source_name','date','sentiment_label','text','summary'])
	all=pd.concat([heb,surveyaid,inmoment])
	filtered = topics[topics['rank'] <= int(limit)]
	
	neutral = []
	positive = []
	negative = []
	count = 0
	ids = {}
	
	for index,row in filtered.iterrows():
		ids[row['topic']]=count
		count+=1
	for index, row in filtered.iterrows():
		sentDict = {}
		for k,v in all[all['text'].str.contains(row['topic']) & (all.text.isna()==False)].groupby("sentiment_label").size().items():
			sentDict[k]=v
		for i in ["negative", "positive", "neutral"]:
			if i in sentDict.keys():
				# nada
				arb=0
			else:
				sentDict[i]=0
		neutral.append({'x':ids[row['topic']],'y':sentDict['neutral'],'label':row['topic']})
		positive.append({'x':ids[row['topic']],'y':sentDict['positive'],'label':row['topic']})
		negative.append({'x':ids[row['topic']],'y':sentDict['negative'],'label':row['topic']})
	return jsonify([{'key':'positive','values':positive},{'key':'neutral','values':neutral},{'key':'negative','values':negative}])

@application.route("/getTopicToSurveys",methods=["GET"])
def returnSurveysToTopic():
	topic = request.args.get("topic")
	heb=pd.read_parquet("static/data/heb.parq",columns=['source_name','date','sentiment_label','text','summary'])
	surveyaid=pd.read_parquet("static/data/survey_aid.parq",columns=['source_name','date','sentiment_label','text','summary'])
	inmoment=pd.read_parquet("static/data/inmoment.parq",columns=['source_name','date','sentiment_label','text','summary'])
	all=pd.concat([heb,surveyaid,inmoment])
	
	results=[]
	for k,v in all[all['text'].str.contains(topic) & (all.text.isna()==False)].groupby("source_name").size().items():
		results.append({'key':k,'y':v})
	return jsonify(results);

@application.route("/dbApi")
def getMlFlow():
	url = 'https://southcentralus.azuredatabricks.net/api/2.0/dbfs/list?path=/Users/e131988/'
	url = 'https://adb-5983567243184270.10.azuredatabricks.net/api/2.0/'+request.args.get("action")
	headers = {'Authorization': 'Bearer '+DATABRICKS_TOKEN,'Content-Type' : 'application/json'}
	r = requests.get(url, headers=headers)
	return jsonify(r.text)

@application.route("/dbfs/<action>")
def getDbfs(action):
	url = 'https://southcentralus.azuredatabricks.net/api/2.0/dbfs/'+action+'?path='+request.args.get("dbfsPath")
	#url = 'https://adb-5983567243184270.10.azuredatabricks.net/api/2.0/'+endpoint+'/'+subendpoint+'/'+action+'?'+paramName+'='+paramValue
	headers = {'Authorization': 'Bearer '+DATABRICKS_TOKEN,'Content-Type' : 'application/json'}
	r = requests.get(url, headers=headers)
	return jsonify(r.text)

@application.route("/contextCreate")
def getContext():
	url = 'https://southcentralus.azuredatabricks.net/api/1.2/contexts/create'
	headers = {'Authorization': 'Bearer '+DATABRICKS_TOKEN,'Content-Type' : 'application/json'}
	r = requests.post(url, headers=headers, json={"language": "scala", "clusterId": "0324-224658-humus803"})
	return jsonify(r.text)

@application.route("/command")
def execCommand():
	url = 'https://southcentralus.azuredatabricks.net/api/1.2/commands/execute'
	headers = {'Authorization': 'Bearer '+DATABRICKS_TOKEN,'Content-Type' : 'application/json'}
	r = requests.post(url, headers=headers, json={"language": "scala", "clusterId": "0324-224658-humus803", "contextId" : request.args.get("contextId"), "command": "sc.parallelize(1 to 10).collect"})
	return jsonify(r.text)

@application.route("/command/status")
def commandStatus():
	url = 'https://southcentralus.azuredatabricks.net/api/1.2/commands/status'
	headers = {'Authorization': 'Bearer '+DATABRICKS_TOKEN,'Content-Type' : 'application/json'}
	r = requests.post(url, headers=headers, json={"clusterId": "0324-224658-humus803", "contextId" : request.args.get("contextId"), "commandId": request.args.get("commandId")})
	return jsonify(r.text)

@application.route("/graph/networkTerms")
def getNetworkGraphTerms():
	results = []
	cat = request.args.get("cat")
	df = pd.read_csv("static/data/network_graph/network_data.csv",header=0)
	df = df[df['term']==cat]
	for i in df['to'].unique():
		results.append(i)
	return jsonify(results)

@application.route("/graph/network")
def getNetworkGraph():
	results = []
	cat = request.args.get("cat")
	term = request.args.get("term")
	df = pd.read_csv("static/data/network_graph/network_data.csv",header=0)
	df = df[(df['term']==cat) & ~(df['to'].str=="it") & ~(df['from'].str=="it") & ~(df['to'].str=="this") & ~(df['from'].str=="this") & ~(df['to'].str=="is") & ~(df['from'].str=="is")]
	if(len(term)>3):
		df = df[df['to']==term]
	nodes = []
	links = []
	nodesSimple = {}
	count = 0
	for i in df.to.unique():
		nodesSimple[i]=count
		#count = count+1
	for i in df['from'].unique():
		if i in nodesSimple:
			arb=0
		else:
			nodesSimple[i]=count
			#count = count+1
	for i in nodesSimple:
		tmp={}
		nodes.append({'id':i,'group':nodesSimple[i]})
	for r,i in df.iterrows():
		if i['to'] != 'this' and i['from'] != 'this' and i['to'] != 'is' and i['from'] != 'is' and i['to'] != 'a' and i['from'] != 'a' and i['to'] != 'be' and i['from'] != 'be' and i['to'] != 'of' and i['from'] != 'of':
			dict={'source':i['from'],'target':i['to'],'value':i['sig'],'positive':i['positive'],'negative':i['negative'],'neutral':i['neutral']}
			links.append(dict)
	return jsonify({'nodes':nodes,'links':links})

@application.route("/download/enrichedData",methods=["GET"])
def downloadEnrichedData():
	heb=pd.read_parquet("static/data/heb.parq",columns=['source_name','date','sentiment_label','text','summary','label','rake_str','sentiment_score'])
	surveyaid=pd.read_parquet("static/data/survey_aid.parq",columns=['source_name','date','sentiment_label','text','summary','label','rake_str','sentiment_score'])
	inmoment=pd.read_parquet("static/data/inmoment.parq",columns=['source_name','date','sentiment_label','text','summary','label','rake_str','sentiment_score'])
	heb = heb[heb['summary'].str.len() > 0]
	surveyaid = surveyaid[surveyaid['summary'].str.len() > 0]
	inmoment = inmoment[inmoment['summary'].str.len() > 0]
	all=pd.concat([heb,surveyaid,inmoment])
	
	try:
		response = make_response()
		output = BytesIO()
		workbook = xlsxwriter.Workbook(output, {'in_memory': True})
		worksheet = workbook.add_worksheet('topic_occurrences')
		row = 0
		col = 0
		for k in ["Source Name","Date","sentiment_score","sentiment_label","Text","Label","rake_str","summary"]:
			worksheet.write(row,col,k)
			col = col+1
		col = 0
		row = row+1
		for index,roww in all.iterrows():
			worksheet.write(row,col,roww['source_name'])
			worksheet.write(row,col+1,str(roww['date']))
			worksheet.write(row,col+2,roww['sentiment_score'])
			worksheet.write(row,col+3,roww['sentiment_label'])
			worksheet.write(row,col+4,roww['text'])
			worksheet.write(row,col+5,roww['label'])
			worksheet.write(row,col+6,roww['rake_str'])
			worksheet.write(row,col+7,roww['summary'])
			row=row+1
		workbook.close()
		output.seek(0)
		response.data = output.read()
		file_name = 'enriched_bulk_data.xlsx'
		mimetype_tuple = mimetypes.guess_type(file_name)
		response_headers = {'Pragma': "public",'Expires': '0','Cache-Control': 'must-revalidate, post-check=0, pre-check=0','Cache-Control': 'private','Content-Type': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','Content-Disposition': 'attachment; filename=\"%s\";' % file_name,'Content-Transfer-Encoding': 'binary','Content-Length': len(response.data)}

		#if not mimetype_tuple[1] is None:
		#	response.update({'Content-Encoding': mimetype_tuple[1]})
		response.headers = response_headers
		#response.set_cookie('fileDownload', 'true', path='/')
		return response
	except Exception as e:
		print(e)
		return "error "+str(e)

@application.route("/getSummary",methods=["GET"])
def getSummaryWithTerms():
	heb=pd.read_parquet("static/data/heb.parq",columns=['source_name','date','sentiment_label','text','summary','label','rake_str','sentiment_score'])
	surveyaid=pd.read_parquet("static/data/survey_aid.parq",columns=['source_name','date','sentiment_label','text','summary','label','rake_str','sentiment_score'])
	inmoment=pd.read_parquet("static/data/inmoment.parq",columns=['source_name','date','sentiment_label','text','summary','label','rake_str','sentiment_score'])
	heb = heb[(heb['summary'].str.len() > 0) & (heb['text'].str.len() > 0)]
	surveyaid = surveyaid[(surveyaid['summary'].str.len() > 0) & (surveyaid['text'].str.len() > 0)]
	inmoment = inmoment[(inmoment['summary'].str.len() > 0) & (inmoment['text'].str.len() > 0)]
	all=pd.concat([heb,surveyaid,inmoment])
	all=all.applymap(lambda s:s.lower() if type(s) == str else s)
	results=[]
	fromt=request.args.get("from").lower()
	to=request.args.get("to").lower()
	term=request.args.get("term").lower()
	for i,r in all[(all['text'].str.contains(fromt)) & (all['text'].str.contains(to))].iterrows():
		results.append({'sentiment_label':r['sentiment_label'],'summary':r['summary']})
	return jsonify(results)

@application.route("/download/getSummary",methods=["GET"])
def getSummaryWithTermsDownload():
	heb=pd.read_parquet("static/data/heb.parq",columns=['source_name','date','sentiment_label','text','summary','label','rake_str','sentiment_score'])
	surveyaid=pd.read_parquet("static/data/survey_aid.parq",columns=['source_name','date','sentiment_label','text','summary','label','rake_str','sentiment_score'])
	inmoment=pd.read_parquet("static/data/inmoment.parq",columns=['source_name','date','sentiment_label','text','summary','label','rake_str','sentiment_score'])
	heb = heb[(heb['summary'].str.len() > 0) & (heb['text'].str.len() > 0)]
	surveyaid = surveyaid[(surveyaid['summary'].str.len() > 0) & (surveyaid['text'].str.len() > 0)]
	inmoment = inmoment[(inmoment['summary'].str.len() > 0) & (inmoment['text'].str.len() > 0)]
	all=pd.concat([heb,surveyaid,inmoment])
	all=all.applymap(lambda s:s.lower() if type(s) == str else s)
	results=[]
	fromt=request.args.get("from").lower()
	to=request.args.get("to").lower()
	term=request.args.get("term").lower()
	for i,r in all[(all['text'].str.contains(fromt)) & (all['text'].str.contains(to))].iterrows():
		results.append({'sentiment_label':r['sentiment_label'],'summary':r['summary']})
	
	try:
		response = make_response()
		output = BytesIO()
		workbook = xlsxwriter.Workbook(output, {'in_memory': True})
		worksheet = workbook.add_worksheet('related_summary')
		row = 0
		col = 0
		for k in ["Source Name","Date","Sentiment Label","Text","Label","Rake Str","Summary"]:
			worksheet.write(row,col,k)
			col = col+1
		col = 0
		row = row+1
		for index,roww in all.iterrows():
			worksheet.write(row,col,roww['source_name'])
			worksheet.write(row,col+1,str(roww['date']))
			worksheet.write(row,col+2,roww['sentiment_label'])
			worksheet.write(row,col+3,roww['text'])
			worksheet.write(row,col+4,roww['label'])
			worksheet.write(row,col+5,roww['rake_str'])
			worksheet.write(row,col+6,roww['summary'])
			row=row+1
		workbook.close()
		output.seek(0)
		response.data = output.read()
		file_name = 'term_summary.xlsx'
		mimetype_tuple = mimetypes.guess_type(file_name)
		response_headers = {'Pragma': "public",'Expires': '0','Cache-Control': 'must-revalidate, post-check=0, pre-check=0','Cache-Control': 'private','Content-Type': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','Content-Disposition': 'attachment; filename=\"%s\";' % file_name,'Content-Transfer-Encoding': 'binary','Content-Length': len(response.data)}

		#if not mimetype_tuple[1] is None:
		#	response.update({'Content-Encoding': mimetype_tuple[1]})
		response.headers = response_headers
		#response.set_cookie('fileDownload', 'true', path='/')
		return response
	except Exception as e:
		print(e)
		return "error "+str(e)


@application.route("/graph/getSentimentDates",methods=["GET"])
def getSentimentDates():
	heb=pd.read_parquet("static/data/heb.parq")
	obn={}
	oby={}
	
	obyPosValuesOrdered=[]
	obyNegValuesOrdered=[]
	obyNeuValuesOrdered=[]
	obnPosValuesOrdered=[]
	obnNegValuesOrdered=[]
	obnNeuValuesOrdered=[]
	order={}
	count=0
	for i,r in heb.iterrows():
		order[str(r['date'])[0:10]]=count
		count=count+1
	
	for i,r in heb.iterrows():
		dt=str(r['date'])[0:10]
		sl=r['sentiment_label']
		obs=r['Own_Brand_Switch']
		if obs == 'N':
			if dt in obn:
				if sl in obn[dt]:
					obn[dt][sl]=obn[dt][sl]+1
				else:
					obn[dt][sl]=1
			else:
				obn[dt]={}
				obn[dt][sl]=1
		if obs == 'Y':
			if dt in oby:
				if sl in oby[dt]:
					oby[dt][sl]=oby[dt][sl]+1
				else:
					oby[dt][sl]=1
			else:
				oby[dt]={}
				oby[dt][sl]=1
	
	obyPosValues=[]
	obyNegValues=[]
	obyNeuValues=[]
	x=0
	for i in oby:
		obyPosValues.append({'x':x,'y':(oby[i]['positive'] if 'positive' in oby[i] else 0),'label':i})
		obyNegValues.append({'x':x,'y':(oby[i]['negative'] if 'negative' in oby[i] else 0),'label':i})
		obyNeuValues.append({'x':x,'y':(oby[i]['neutral'] if 'neutral' in oby[i] else 0),'label':i})
		x=x+1
	obnPosValues=[]
	obnNegValues=[]
	obnNeuValues=[]
	x=0
	for i in obn:
		obnPosValues.append({'x':x,'y':(obn[i]['positive'] if 'positive' in obn[i] else 0),'label':i})
		obnNegValues.append({'x':x,'y':(obn[i]['negative'] if 'negative' in obn[i] else 0),'label':i})
		obnNeuValues.append({'x':x,'y':(obn[i]['neutral'] if 'neutral' in obn[i] else 0),'label':i})
		x=x+1
	
	sort=False
	if sort:
		print(str(len(obyPosValues)) + " " + str(len(obyNegValues))+ " "+str(len(obyNeuValues)))
		print(str(len(obnPosValues)) + " " + str(len(obnNegValues))+ " "+str(len(obnNeuValues)))
		for dt in order:
			x=order[dt]
			print(x)
			for obj in obyPosValues:
				if obj['label']==dt:
					obyPosValuesOrdered.append({'x':x,'y':obj['y'],'label':dt})
			for obj in obyNegValues:
				if obj['label']==dt:
					obyNegValuesOrdered.append({'x':x,'y':obj['y'],'label':dt})
			for obj in obyNeuValues:
				if obj['label']==dt:
					obyNeuValuesOrdered.append({'x':x,'y':obj['y'],'label':dt})
		for dt in order:
			x=order[dt]
			for obj in obnPosValues:
				if obj['label']==dt:
					obnPosValuesOrdered.append({'x':x,'y':obj['y'],'label':dt})
			for obj in obnNegValues:
				if obj['label']==dt:
					obnNegValuesOrdered.append({'x':x,'y':obj['y'],'label':dt})
			for obj in obnNeuValues:
				if obj['label']==dt:
					obnNeuValuesOrdered.append({'x':x,'y':obj['y'],'label':dt})
		return jsonify({'oby':[{'key':'positive','values':obyPosValuesOrdered},{'key':'neutral','values':obyNeuValuesOrdered},{'key':'negative','values':obyNegValuesOrdered}],'obn':[{'key':'positive','values':obnPosValuesOrdered},{'key':'neutral','values':obnNeuValuesOrdered},{'key':'negative','values':obnNegValuesOrdered}]})
	return jsonify({'oby':[{'key':'positive','values':obyPosValues},{'key':'neutral','values':obyNeuValues},{'key':'negative','values':obyNegValues}],'obn':[{'key':'positive','values':obnPosValues},{'key':'neutral','values':obnNeuValues},{'key':'negative','values':obnNegValues}]})

if __name__ == "__main__":
	application.run(host="0.0.0.0",debug=True)