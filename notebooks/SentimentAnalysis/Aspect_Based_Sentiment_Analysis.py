# Databricks notebook source
# MAGIC %md ### What is Aspect Based Sentiment Analysis ? 

# COMMAND ----------

# MAGIC %md 
# MAGIC Aspect-based sentiment analysis (ABSA) is a text analysis technique that categorizes data by aspect and identifies the sentiment attributed to each one. Aspect-based sentiment analysis can be used to analyze customer feedback by associating specific sentiments with different aspects of a product or service.
# MAGIC 
# MAGIC When we talk about aspects, we mean the attributes or components of a product or service e.g. “the user experience of a new product,” “the response time for a query or complaint,” or “the ease of integration of new software.”

# COMMAND ----------

# MAGIC %md #### Import Libraries 

# COMMAND ----------

#dataframe and calculations
import pandas as pd
import numpy as np
#regular expression operations 
import re
#text library 
import nltk
from nltk.corpus import stopwords, sentiwordnet as swn
from nltk.stem import WordNetLemmatizer
from nltk import ngrams
nltk.download("all") 
#feature engineering
from sklearn.feature_extraction.text import CountVectorizer
#topic modeling 
from sklearn.decomposition import LatentDirichletAllocation
#python collection module for aggregate operations like counter() etc 
import collections
#sentiment anaysis 
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
#for visualizations 
import matplotlib.pyplot as plt
from wordcloud import WordCloud

# COMMAND ----------

# MAGIC %run "./stop_words_list"

# COMMAND ----------

# MAGIC %run  "./wn_affect" 

# COMMAND ----------

# MAGIC %md #### Import Data 
# MAGIC 
# MAGIC Here I filter to only Own Brand Quality labels. We want to do an analysis about what customers say when talking about product quality. 

# COMMAND ----------

df = spark.sql("""
                select feedback_description as text 
                ,case when feedback_category like '%Own Brand Quality%' then 1 else 0 end as label
               --, feedback_category
               from curated.customer_feedback
               where  source_name = 'Survey-Aid' and feedback_category like '%Own Brand Quality%'
""").toPandas().dropna()
df.head()

# COMMAND ----------

# MAGIC %md #### Data Preprocessing / Cleaning 

# COMMAND ----------

# case text as lowercase, remove punctuation, remove extra whitespace in string and on both sides of string
df['remove_lower_punct'] = df['text'].str.lower().str.replace("'", '').str.replace('[^\w\s]', ' ').str.replace(" \d+", " ").str.replace(' +', ' ').str.strip()

display(df.head(10))

# COMMAND ----------

# MAGIC %md #### Calculate sentiment scores and sentiment classes (positive, negative, neutral) and add them to main dataframe 
# MAGIC 
# MAGIC Note that this looks at overall sentiment. Can we find a way to add our dictionary on the sentiment anlalyzer? 

# COMMAND ----------

# apply sentiment analysis
analyser = SentimentIntensityAnalyzer()

sentiment_score_list = []
sentiment_label_list = []

for i in df['remove_lower_punct'].values.tolist():
    sentiment_score = analyser.polarity_scores(i)

    if sentiment_score['compound'] >= 0.05:
        sentiment_score_list.append(sentiment_score['compound'])
        sentiment_label_list.append('Positive')
    elif sentiment_score['compound'] > -0.05 and sentiment_score['compound'] < 0.05:
        sentiment_score_list.append(sentiment_score['compound'])
        sentiment_label_list.append('Neutral')
    elif sentiment_score['compound'] <= -0.05:
        sentiment_score_list.append(sentiment_score['compound'])
        sentiment_label_list.append('Negative')
    
df['sentiment'] = sentiment_label_list
df['sentiment score'] = sentiment_score_list

display(df.head(10))

# COMMAND ----------

# MAGIC %md #### Perform tokenization, split text strings into tokens (words)

# COMMAND ----------

# tokenise string
df['tokenise'] = df.apply(lambda row: nltk.word_tokenize(row[2]), axis=1)

display(df.head(10))

# COMMAND ----------

# MAGIC %md #### Remove Stopwords

# COMMAND ----------

#initiate stopwords from nltk
stop_words = stopwords.words('english')

# add additional missing terms
#stop_words.extend(stop_words_list) 

# remove stopwords
df['remove_stopwords'] = df['tokenise'].apply(lambda x: [item for item in x if item not in stop_words])

display(df.head(10))

# COMMAND ----------

# MAGIC %md #### Get lemma version of the tokens 

# COMMAND ----------

# initiate nltk lemmatiser
wordnet_lemmatizer = WordNetLemmatizer()

# lemmatise words
df['lemmatise'] = df['remove_stopwords'].apply(lambda x: [wordnet_lemmatizer.lemmatize(y) for y in x]) 

display(df.head(10))

# COMMAND ----------

# MAGIC %md #### Lemmatized tokens convert them into Count Vectors 

# COMMAND ----------

# initialise the count vectorizer
vectorizer = CountVectorizer(analyzer = 'word', ngram_range = (2, 2))
                            
# join the processed data to be vectorised
vectors = []

for index, row in df.iterrows():
    vectors.append(", ".join(row[7]))

vectorised = vectorizer.fit_transform(vectors)

# COMMAND ----------

# MAGIC %md #### Perform Topic Modeling with LDA 

# COMMAND ----------

# initialise LDA Model
lda_model = LatentDirichletAllocation(n_components = 10, # number of topics
                                      random_state = 10,          # random state
                                      evaluate_every = -1,      # compute perplexity every n iters, default: Don't
                                      n_jobs = -1,              # Use all available CPUs
                                      )

lda_output = lda_model.fit_transform(vectorised)

# column names
topic_names = ["Topic" + str(i) for i in range(1, lda_model.n_components + 1)]

# make the pandas dataframe
df_document_topic = pd.DataFrame(np.round(lda_output, 2), columns = topic_names)

# get dominant topic for each document
dominant_topic = (np.argmax(df_document_topic.values, axis=1)+1)
df_document_topic['Dominant_topic'] = dominant_topic

# join to original dataframes
df = pd.merge(df, df_document_topic, left_index = True, right_index = True, how = 'outer')
df.head(5)

# COMMAND ----------

# index names
docnames = ['Doc' + str(i) for i in range(len(df))]

# Make the pandas dataframe
df_document_topic = pd.DataFrame(np.round(lda_output, 2), columns=topic_names, index=docnames)

# Get dominant topic for each document
dominant_topic = np.argmax(df_document_topic.values, axis=1)
df_document_topic['dominant_topic'] = dominant_topic

# Topic-Keyword Matrix
df_topic_keywords = pd.DataFrame(lda_model.components_)

# Assign Column and Index
df_topic_keywords.columns = vectorizer.get_feature_names()
#df_topic_keywords.index = topic_names

df_topic_no = pd.DataFrame(df_topic_keywords.idxmax())
df_scores = pd.DataFrame(df_topic_keywords.max())

tmp = pd.merge(df_topic_no, df_scores, left_index=True, right_index=True)
tmp.columns = ['topic', 'relevance_score']

tmp

# COMMAND ----------

all_topics = []

for i in tmp['topic'].unique():    
    tmp_1 = tmp.loc[tmp['topic'] == i].reset_index()
    tmp_1 = tmp_1.sort_values('relevance_score', ascending=False).head(1)

    tmp_1['topic'] = tmp_1['topic'] + 1
    
    tmp_2 = []
    tmp_2.append(tmp_1['topic'].unique()[0])
    tmp_2.append(list(tmp_1['index'].unique()))
    all_topics.append(tmp_2)

all_topics = pd.DataFrame(all_topics, columns=['Dominant_topic', 'topic_name'])
display(all_topics)

# COMMAND ----------

results = df.groupby(['Dominant_topic', 'sentiment']).count().reset_index()

results = results.merge(all_topics, on='Dominant_topic')
results['topic_name'] = results['topic_name'].apply(', '.join)

graph_results = results[['topic_name', 'sentiment', 'sentiment score']]
graph_results = graph_results.pivot(index='topic_name', columns='sentiment', values='sentiment score').reset_index()

graph_results.set_index('topic_name', inplace=True)

graph_results

# COMMAND ----------

import matplotlib.pyplot as plt
fig = graph_results.plot.bar(rot=90, figsize=(10,10))
fig.figure.savefig('sentiment_analysis.png', bbox_inches='tight')

# COMMAND ----------

# MAGIC %md #### Perform Part of Speech Tagging 

# COMMAND ----------

from nltk.corpus import wordnet

def get_wordnet_pos(treebank_tag):

    if treebank_tag.startswith('J'):
        return wordnet.ADJ
    elif treebank_tag.startswith('V'):
        return wordnet.VERB
    elif treebank_tag.startswith('N'):
        return wordnet.NOUN
    elif treebank_tag.startswith('R'):
        return wordnet.ADV
    else:
        pass



# COMMAND ----------

positive_words = []
negative_words = []

for i in df['Dominant_topic'].unique():
    if i == 1:
        tmp_1 = df.loc[df['Dominant_topic'] == i]
                
        for j in tmp_1['tokenise'].values.tolist():
            for p in nltk.pos_tag(j):
                get_pos_tag = get_wordnet_pos(p[1])
                if type(get_pos_tag) == str:
                    try:        
                        synset = swn.senti_synset(p[0] + '.' + get_pos_tag +'.01')
                        if synset.obj_score() <= 0.49:
                            if synset.pos_score() > synset.neg_score() and p[0] in wn_affect:
                                    positive_words.append(p[0])
                            elif synset.neg_score() > synset.pos_score() and p[0] in wn_affect:
                                    negative_words.append(p[0])      
                    except:
                        pass

# COMMAND ----------

wn_affect

# COMMAND ----------

unique_positive_words = list(set(positive_words))
unique_negative_words = list(set(negative_words))

count_positive_words = []
count_negative_words = []

for i in unique_positive_words:
    counter = [i, positive_words.count(i)]
    count_positive_words.append(counter)

for i in unique_negative_words:
    counter = [i, negative_words.count(i)]
    count_negative_words.append(counter)    
    
positive_words = pd.DataFrame(count_positive_words, columns = ['word', 'score'])
negative_words = pd.DataFrame(count_negative_words, columns = ['word', 'score'])

positive_words.sort_values('score', ascending=False, inplace = True)
negative_words.sort_values('score', ascending=False, inplace = True)

# COMMAND ----------

unique_positive_words

# COMMAND ----------

from wordcloud import WordCloud
word_dict = {}
for k, v in positive_words.values:
    word_dict[k] = v

wordcloud = WordCloud()
wordcloud.generate_from_frequencies(frequencies=word_dict)
plt.figure(figsize=(20,10))
plt.imshow(wordcloud, interpolation="bilinear")
plt.axis("off")
plt.savefig('positive_words.png')
plt.show()

# COMMAND ----------

word_dict

# COMMAND ----------

word_dict = {}
for k, v in negative_words.values:
    word_dict[k] = v
    
wordcloud.generate_from_frequencies(frequencies=word_dict)
plt.figure(figsize=(20,10))
plt.imshow(wordcloud, interpolation="bilinear")
plt.axis("off")
plt.savefig('negative_words.png')
plt.show()

# COMMAND ----------

word_dict