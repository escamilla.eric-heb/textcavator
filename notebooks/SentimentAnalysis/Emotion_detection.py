# Databricks notebook source
pip install text2emotion

# COMMAND ----------

import text2emotion as te

# COMMAND ----------

df = spark.sql("""
                select feedback_description as text 
                ,case when feedback_category like '%Own Brand Quality%' then 1 else 0 end as label
               --, feedback_category
               from curated.customer_feedback
               where  source_name = 'Survey-Aid' and feedback_category like '%Own Brand Quality%'
""").toPandas().dropna()
df.head()

# COMMAND ----------

df['emmotion'] =df['text'].apply(lambda x: [te.get_emotion(y) for y in x])
display(df.head(10))

# COMMAND ----------

text = "I am very disgusted by this product"
te.get_emotion(text)

# COMMAND ----------

text = "I almost swallowed a piece of glass! wth!"
te.get_emotion(text)

# COMMAND ----------

text = "Hi HEB, I just heard that you opened a new store near Ft. Worth and I got so excited, I screamed in the car. You see, I moved to North Texas about 7 months ago. I moved for my dream job and by moving, I truly changed my life. I got out of a bad relationship, got back to my passion (working with animals at the SPCA of Texas in Dallas), and moved close to some really great friends. The worst problem that I have is my HEB withdrawal. It is severe. I am not sure I can make it. I have tried Kroger, Alberstons, WinCo, Target, WalMart, Tom Thumb and Aldi - just SEARCHING for anything comparable. Alas, nothing compares to you, HEB. I am living with friends at the moment and looking for a place to buy a house that is closer to work. I am writing to find out if you have any more plans to open HEB in DFW? I am not even joking when I say that I will look for a house based on your plans. Please tell me that more HEB's are coming. "
te.get_emotion(text)

# COMMAND ----------

text = "I’m calling from the H-E-B parking lot. I purchased my groceries. The cashier had an attitude with the lady in front of me. I was polite enough with her then when it came time to do my coupons. I didn’t realize the granola bar coupon called for two. I apologized and she became really rude and angry and said “are you going to go and get another one or do you expect me to?” Finally, she called the manager, and he actually politely went and got them. As soon as he left she became rude again. He finally came back, and my card was in the chip reader. “Are you paying with WIC or are you using a debit card?” I didn’t have any WIC items. There was nothing in my order that would infer WIC. What would make you jump to that conclusion? That I’m black? I have money! I just said, “it is a regular debit card”. That was just blatantly disrespectful and said loud enough for all of the other customers in the area to hear. I’d rather The receipt said Daejah W. but her nametag said Mya"
te.get_emotion(text)

# COMMAND ----------

text = "My husband got really, really sick a few days ago. Diarrhea and nausea a whole day. The next day I noticed a huge bug in the salad we had been eating. I brought everything back to the store (Braker Ln, Austin #17). That was Wednesday. They told me there was nothing the could do there and not even a report of the illness and gave me the number for the corporate office. This has happened twice with your smaller boxes. This time it was a really big and gross bug. I can send pictures; it is like a roach or something with a white web like substance all around it like it was there for a while. H-E-B Organics Baby Spring Mix. I took the box and showed them the bug, but I kept the bug in my freezer. I did receive a refund for the product. Best if Used by May 16 TFRS120B11A 20:28"
te.get_emotion(text)

# COMMAND ----------

text = "My husband got really, really sick a few days ago. Diarrhea and nausea a whole day.he next day I noticed a huge bug in the salad we had been eating."
te.get_emotion(text)

# COMMAND ----------

text = "I just got checked out by Jon in 15 items of less. His attitude was pleasant and helpful. even tho I had 2 orders (mine and my mom's) and lots of coupons. He was a joy at the end of a long ay. I really apprecited his big smile and easy manner. (Some cashiers are pretty grumpy and understandably so give some o the customer and the job pressures of this position"
te.get_emotion(text)