# Databricks notebook source
textDF = spark.sql("select * from curated.customer_feedback where source_name = 'Survey-Aid' ")
textDF = textDF.select( "customer_feedback_id", "feedback_category", "feedback_description")
textDF.cache().count()

# COMMAND ----------

from pyspark.ml.feature import Tokenizer, RegexTokenizer
# SparkML's Tokenizer function 
tokenizer = Tokenizer(inputCol="feedback_description", outputCol="tokens")
#tokenizer = RegexTokenizer(inputCol="feedback_description", outputCol="Tokens", pattern="\\W")
tokenizedDF = tokenizer.transform(textDF)
display(tokenizedDF.limit(20))

# COMMAND ----------

from pyspark.ml.feature import StopWordsRemover

# Use default stopwords and add "br" to stopwords list
stopWords = StopWordsRemover().getStopWords() 
stopwordsRemover = StopWordsRemover(inputCol="tokens", outputCol="CleanTokens", stopWords=stopWords)
processedDF = stopwordsRemover.transform(tokenizedDF)

display(processedDF.limit(5))

# COMMAND ----------

from pyspark.ml import Pipeline 
from pyspark.ml.feature import HashingTF, IDF, Tokenizer, Normalizer

hashingTF = HashingTF(inputCol="CleanTokens", outputCol="TermFrequencies") # Or use CountVectorizer
idf = IDF(inputCol="TermFrequencies", outputCol="TFIDFScore")
normalizer = Normalizer(inputCol="TFIDFScore", outputCol="TFIDFScoreNorm", p=2) # Normalize L2

# Adding functions into a pipeline to streamline calling process
tfidfPipeline = Pipeline(stages = [hashingTF, idf, normalizer])
tfidfModel = tfidfPipeline.fit(processedDF)
tfidfDF = tfidfModel.transform(processedDF).drop("TFIDFScore")

display(tfidfDF.drop("Tokens").limit(20))

# COMMAND ----------

from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer

analyzer = SentimentIntensityAnalyzer()

def get_nltk_sentiment_score(text):
  return analyzer.polarity_scores(text)["compound"]

# Example usage
get_nltk_sentiment_score("I’ve been having massive quality issues with Hill Country fare frozen chicken. It’s been getting progressively worse for the last several months and it has gotten to the point that I don’t even want to make chicken dishes because I don’t want to use the chicken I’ve been wasting all this money on. I need to get into contact with whatever customer care they have. There’s no contact information on the bag.")

# COMMAND ----------

from pyspark.sql.types import *
from pyspark.sql.functions import col

sentiment_nltk_udf = udf(lambda text: get_nltk_sentiment_score("feedback_description"), FloatType())
sentimentDF = textDF.withColumn("sentimentNLTKScore", sentiment_nltk_udf(col("feedback_description")))

display(sentimentDF)

# COMMAND ----------

