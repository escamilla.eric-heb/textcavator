// Databricks notebook source
import org.apache.spark.sql.functions._
import org.apache.spark.sql.expressions._
import org.apache.spark.sql.types._

// read in output from NetworkGraph notebook
var df=spark.read.option("header",true).csv("/Users/e131988/formed.csv").withColumn("sig",col("sig").cast(DoubleType))
.withColumn("positive",col("positive").cast(LongType))
.withColumn("negative",col("negative").cast(LongType))
.withColumn("neutral",col("neutral").cast(LongType))
/*
newrows and someSchema to define the rows going into the newrows variable (this is to pick up more negative words)
*/
val newdata=new java.util.ArrayList[Row]
val newrows=new java.util.ArrayList[Row]
val someSchema = StructType(Seq(
      StructField("term", StringType, false),
      StructField("from", StringType, false),
      StructField("to", StringType, false),
    StructField("sig", DoubleType, false),
  StructField("positive", LongType, false),
  StructField("negative", LongType, false),
  StructField("neutral", LongType, false)
    ))

//  union necessary tables and columns
val heb=spark.read.parquet("/Users/e131988/heb.parq").select("sentiment_label","text","date").withColumn("text",lower(col("text")))
val surveyAid=spark.read.parquet("/Users/e131988/survey_aid.parq").select("sentiment_label","text","date").withColumn("text",lower(col("text")))
val inmoment=spark.read.parquet("/Users/e131988/inmoment.parq").select("sentiment_label","text","date").withColumn("text",lower(col("text")))
val all=heb.union(surveyAid).union(inmoment)

val nogowords=java.util.Arrays.asList("it","a","the","i","with","and","my","has","this")
val negwords=List("bug","bugs","mold","molding","hurt","hurts","struggle","struggled","bug","bugs","roach","insect","insects","roaches","pain","indigestion","sick","sickening","debris","shard","sharp","cut","sludge")
val sentiments=List("positive","negative","neutral")

val groupedTermFrom = df.groupBy("term","from").count()
groupedTermFrom.take(groupedTermFrom.count.toInt).foreach(r=>{
  for(a <- negwords){
    newrows.add(Row(r.getString(0),r.getString(1),a,-1.0,0L,0L,0L))
  }
})
val gnd=spark.createDataFrame(newrows,someSchema).withColumn("sig",col("sig").cast(DoubleType))
.withColumn("positive",col("positive").cast(LongType))
.withColumn("negative",col("negative").cast(LongType))
.withColumn("neutral",col("neutral").cast(LongType))
df = df.union(gnd).withColumn("sig",col("sig").cast(DoubleType))
.withColumn("positive",col("positive").cast(LongType))
.withColumn("negative",col("negative").cast(LongType))
.withColumn("neutral",col("neutral").cast(LongType))

df.take(df.count.toInt).foreach(r=>{
  var term=""
  var from=""
  var to=""
  var sig=0.0
  scala.util.Try{
    term=r.getAs[String]("term")
    from=r.getAs[String]("from")
    to=r.getAs[String]("to")
    sig=r.getAs[Double]("sig")
  }recover{
    case e=>{println(e.getMessage)}
  }
  scala.util.Try{
    val groupedSentimentLabels=all.where(col("text").contains(term) && col("text").contains(from) && col("text").contains(to)).groupBy("sentiment_label").count
    val map=new java.util.HashMap[String,Long]
    groupedSentimentLabels.take(groupedSentimentLabels.count.toInt).foreach(ir=>{
      map.put(ir.getString(0),ir.getLong(1))
    })
    for(s <- sentiments)
      if(!map.containsKey(s)) map.put(s,0)
    if(sig < 0){
      println("Processing "+from + " " + to)
      var continue=false
      var ssum=0L
      for(s <- sentiments)
        if(map.get(s) != 0){
          continue=true
          ssum = ssum + map.get(s)
        }
      if(continue) {
        sig = ssum
      } else {
        println("Nothing found for "+from+" "+to)
      }
    }
    if(sig > -1)
      newdata.add(Row(term,from,to,sig,map.get("positive"),map.get("negative"),map.get("neutral")))
  } recover {
    case e=>{println(r + " "+ e.getMessage)}
  }
})
spark.createDataFrame(newdata,someSchema).coalesce(1).write.format("csv").option("header",true).save("/Users/e131988/formed_new.csv")